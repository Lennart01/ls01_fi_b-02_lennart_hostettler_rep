﻿
import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args) {
    	while(true) {
    		Scanner tastatur = new Scanner(System.in);
      
    		double zuZahlenderBetrag; 
    		double eingezahlterGesamtbetrag;
    		double rückgabebetrag;
    		String[] tarif = new String[10];
    		int[] tarifPreis = new int[10];
       
       //Fahrkarte auswählen
       //======================
    		zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
      
       // Geldeinwurf
       // -----------
    		eingezahlterGesamtbetrag = fahrkartenBezahlen(tastatur, zuZahlenderBetrag);

       // Fahrscheinausgabe
       // -----------------
    		fahrkartenAusgeben(tastatur);
       
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
    		rückgabebetrag = rueckgeldAusgeben(tastatur, eingezahlterGesamtbetrag, zuZahlenderBetrag);
    		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
    	}
    }

	public static double rueckgeldAusgeben(Scanner tastatur, double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		
	       double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(rückgabebetrag > 0.0)
	       {
	    	   System.out.print("Der Rückgabebetrag in Höhe von ");
	    	   System.out.printf("%.2f",rückgabebetrag);
	    	   System.out.println(" EURO");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.045)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.");
		
		return 0;
	}

	private static void fahrkartenAusgeben(Scanner tastatur) {
		
	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	}

	static double fahrkartenBezahlen(Scanner geldeinwurf, double zuZahlen) {
		
		double eingezahlt = 0.0;
		double eingeworfeneMünze;
		eingezahlt = 0.0;
	       while(eingezahlt < zuZahlen)
	       {
	    	   System.out.print("Noch zu zahlen: " );
	    	   System.out.printf("%2.2f",zuZahlen - eingezahlt);
	    	   System.out.print(" Euro\n");
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = geldeinwurf.nextDouble();
	           eingezahlt += eingeworfeneMünze;
	       }	
		
		return eingezahlt;
	}

	static double fahrkartenbestellungErfassen(Scanner eingabe) {
		// die array liste ist wesentlich übersichtlicher, einfacher und besser wartbar als als meine switch case anweisung davor
		String[] tarif = new String[11];
		double[] tarifPreis = new double[11];
		
	    tarif[1] = " 1) Einzelfahrschein Berlin AB           [ 2,90 €]";
		tarif[2] = " 2) Einzelfahrschein Berlin BC           [ 3,30 €]";
		tarif[3] = " 3) Einzelfahrschein Berlin ABC          [ 3,60 €]";
		tarif[4] = " 4) Kurzstrecke                          [ 1,90 €]";
		tarif[5] = " 5) Tageskarte Berlin AB                 [ 8,60 €]";
		tarif[6] = " 6) Tageskarte Berlin BC                 [ 9,00 €]";
		tarif[7] = " 7) Tageskarte Berlin ABC                [ 9,60 €]";
		tarif[8] = " 8) Kleingruppen-Tageskarte Berlin AB    [23,50 €]";
		tarif[9] = " 9) Kleingruppen-Tageskarte Berlin BC    [24,30 €]";
		tarif[10] = "10) Kleingruppen-Tageskarte Berlin ABC   [24,90 €]";
		
		tarifPreis[1] = 2.90;
		tarifPreis[2] = 3.30;
		tarifPreis[3] = 3.60;
		tarifPreis[4] = 1.90;
		tarifPreis[5] = 8.60;
		tarifPreis[6] = 9.00;
		tarifPreis[7] = 9.60;
		tarifPreis[8] = 23.50;
		tarifPreis[9] = 24.30;
		tarifPreis[10] = 24.90;
		
		int fahrkartenTarif;
		int anzahl;
		
		double gesamtpreis;
		
		System.out.println("Fahrkartenbestellvorgang:");
		System.out.println("=========================");
		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin ABC aus: \n");
		for(int i = 1; i < tarif.length; i++) {
			System.out.print(tarif[i]+ " ");
			System.out.println();		
		}
		
		 
		System.out.print("\nIhre Wahl: ");  
		fahrkartenTarif = eingabe.nextInt();
		 
	       for(int i = 0; i < 1; i++) {
	    	   if(fahrkartenTarif > 3) {	    		   
	    		   System.out.println(">> falsche Eingabe <<");
	    		   System.out.print("Ihre Wahl: ");  
	    		   fahrkartenTarif = eingabe.nextInt();
	    	   }
	    	   
	    	   
	    		  System.out.print("Gewünschte Anzahl der Fahrkarten: ");
	   	          anzahl = eingabe.nextInt();	   	       
	   	          gesamtpreis = anzahl * tarifPreis[fahrkartenTarif];
	   	          return gesamtpreis; 
	    	  }
	    
	       anzahl = eingabe.nextInt();	       
	       gesamtpreis = anzahl * fahrkartenTarif;
		return gesamtpreis;
	   
	}   
}